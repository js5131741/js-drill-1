function problem3(inventory) {
    const carModels = [];
    for (const car of inventory) {
      carModels.push(car.car_model);
    }
    carModels.sort();
    return carModels;
  }
  
  module.exports = problem3;
  