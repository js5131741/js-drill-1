function problem5(inventory) {
    const carYears = [];
    for (const car of inventory) {
      carYears.push(car.car_year);
    }
    const yearsOlderThan2000 = []
    for (const year of carYears){
        if (year < 2000) {
            yearsOlderThan2000.push(year);
        }
    }
    return yearsOlderThan2000;
}
    module.exports = problem5;
  