function problem6(inventory) {
    const filteredCars = []
    for (const car of inventory){
      if (car.car_make === 'BMW' || car.car_make === 'Audi'){
        filteredCars.push(car)
      }
    }
    return filteredCars;
  }
  
  module.exports = problem6;
  