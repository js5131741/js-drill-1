const problem6 = require('../problem6');
const inventory = require('../Inventory'); // Import inventory data

const BMWAndAudiCars = problem6(inventory);

if (BMWAndAudiCars.length > 0) {
  console.log('BMW and Audi cars:');
  console.log(JSON.stringify(BMWAndAudiCars, null, 2));
} else {
  console.log('No BMW or Audi cars found in the inventory.');
}