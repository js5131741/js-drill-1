
function problem1(inventory, id) {
    const car = inventory.find((item) => item.id === id);
    return car;
}

module.exports = problem1